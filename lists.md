This is an example of a regular ordered list in Markdown:

Source code
```markdown
1. Item A
1. Item B
   1. Step a
      1. Stage I
      1. Stage II
         1. Part x
         1. Part y
         1. Part w
      1. Stage III
   1. Step b
   1. Step c
1. Item C
```

which renders as:

1. Item A
1. Item B
   1. Step a
      1. Stage I
      1. Stage II
         1. Part x
         1. Part y
         1. Part w
      1. Stage III
   1. Step b
   1. Step c
1. Item C
